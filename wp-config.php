<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressdbuser');

/** MySQL database password */
define('DB_PASSWORD', 'VagrantDBUser123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8_L-gDShxm:]2sQ3taiEO2/aOH iUz;]`C7Ubc^E4mQ>#]=b^#lKJzW`GB*D%Or.');
define('SECURE_AUTH_KEY',  '`%m~*-tB},Y$+@@$tL3dk]eYq*$7etEsr?C5|ry~[O29%+k;))=s@P.>_Hc`}@o+');
define('LOGGED_IN_KEY',    'keI.BvIy+eq9(byvOn90otgPe-Et.mhgP$)n[zY98yN<s&=8Bwz;~ETVSE@^jEuu');
define('NONCE_KEY',        'g*(4v)J$!cVi{Z4X[E5)sV=24q*vE_LE{hH%UE3WB<9V!4$X9SbK*sjP1@ysy#^D');
define('AUTH_SALT',        'Xd6B-LWj<&/S$Ikt)eq?esMHXZ+~Sr+kts<QUkd0*trs7d+?Y!nXrM$nQ-E3fo#G');
define('SECURE_AUTH_SALT', '?E>PPA(J`=_8>a0Iv0?(CYzgAjkZj|nm`vLJoS?{Ry++|H..A0_Tn|+nsy?ImhF^');
define('LOGGED_IN_SALT',   'h/H6nut^9s%[j$dGHV|Y:G73Rclcwl`a26!&5B,_/Waz-t=XB^/0m%.5Y2+Z$(ZQ');
define('NONCE_SALT',       '($D^0!KjAAp|Znt+o+^|;gGslLBweU*>l&CYfdP|7U|<mt}6? vZoV-aE9l 78zs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
