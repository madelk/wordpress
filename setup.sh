#!/bin/sh
DBPASSWD=Vagrant123

echo "Updating and upgrading"
apt-get update

if [ ! -e "/etc/apache2" ]
then
    echo "Installing LAMP"
    echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections 
    echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections 
    sudo apt-get install -y lamp-server^
fi

# download wordpress
if [ ! -e "/var/www/index.php" ]
then
    echo "Downloading Wordpress"
    cd /var/www
    wget --quiet https://wordpress.org/latest.tar.gz
    tar --strip-components=1 -xzf `basename latest.tar.gz`
    rm `basename latest.tar.gz`
fi

cd /var/www
rm index.html
apt-get install php5-gd -y

cp /vagrant/wp-config.php /var/www/wp-config.php
# mkdir sites/default/files
sudo chown -R www-data:www-data .
chmod 755 /var/www/wp-content/
# chmod 666 sites/default/settings.php

service apache2 restart

mysql --user=root --password=Vagrant123 -e "CREATE DATABASE wordpress CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql --user=root --password=Vagrant123 -e "CREATE USER wordpressdbuser@localhost IDENTIFIED BY 'VagrantDBUser123';"
mysql --user=root --password=Vagrant123 -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON wordpress.* TO 'wordpressdbuser'@'localhost' IDENTIFIED BY 'VagrantDBUser123';"

echo "site should now be available at localhost:8082"